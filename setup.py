"""Setup file for the pap project"""

import os
import importlib
from setuptools import setup, find_packages

package = importlib.import_module(find_packages()[0])

setup(
    name=package.__name__,
    version=package.__version__,
    author=package.__author__,
    description=package.__doc__,
    license='MIT License',
    packages=[package.__name__],
	url='https://gitlab.com/kwilkins/pap',
	entry_points = {
		'console_scripts': ['pap=pap:main'],
	},
	install_requires=[
	    'packaging',
	],
)
